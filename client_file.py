import socket
import threading

HOST = socket.gethostbyname(socket.gethostname())  # will get IP Address
PORT = 65432

HEADER = 64
FORMAT = "utf-8"
ADDR = (HOST, PORT)
DISCONNECT_MSG = "!DISCONNECTED"

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(ADDR)


def send(msg):
    message = msg.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b" " * (HEADER - len(send_length))
    client.send(send_length)
    client.send(message)


send("Hello World!!!")

send("Hi Sneha!!")
input()
send(DISCONNECT_MSG)
