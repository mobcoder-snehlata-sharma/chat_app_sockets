import socket
import threading

HOST = socket.gethostbyname(socket.gethostname())  # will get IP Address
PORT = 65432

HEADER = 64
FORMAT = "utf-8"
ADDR = (HOST, PORT)
DISCONNECT_MSG = "!DISCONNECTED"
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


server.bind(ADDR)


def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected...")
    connected = True

    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg == DISCONNECT_MSG:
                connected = False

            print(f"[{addr} - {msg}]")

    conn.close()


def start():
    server.listen()  # listen clients
    print(f"Listening Server on {HOST} ..")

    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS]  {threading.activeCount() -1}")


# def disconnect():
#     pass


print("[SERVER] is starting.....")
start()
